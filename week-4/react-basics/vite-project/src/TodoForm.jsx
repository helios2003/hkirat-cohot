import { useState } from "react";

const TodoForm = () => {
    const [todo, SetTodo] = useState([])
    const handleTodos= () => {
        const title_input = document.getElementById("title");
        const description_input = document.getElementById("description");
        if (title_input && description_input) {
            let title = title_input.value
            let description = description_input.value
            SetTodo([...todo, {title, description}])
            title_input.value = "";
            description_input.value = "";
        }
    }
    const DoneTodo = (index) => {
        const updatedTodos = [...todo];
        updatedTodos[index].done = true
        SetTodo(updatedTodos);
    }
    const DeleteTodos = (index) => {
        const updatedTodos = [...todo];
        updatedTodos.splice(index, 1);
        SetTodo(updatedTodos);
    }
    return (
        <>
            <input id="title" placeholder="enter your title"></input>
            <input id="description" placeholder="enter your description"></input>
            <button onClick={handleTodos}>ADD</button>
            {todo.map((todo, index) => (
                <div key={index}>
                    <p>Title: {todo.title}</p>
                    <p>Description: {todo.description}</p>
                    <button onClick={() => DoneTodo(index)}>{todo.done ? "Done" : "Mark as Done"}</button>
                    <button onClick={() => DeleteTodos(index)}>Delete</button>
                </div>
            ))}
        </>
    )
}

export default TodoForm;