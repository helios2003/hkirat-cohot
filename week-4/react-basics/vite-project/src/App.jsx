import { useState } from 'react'
import TodoForm from './TodoForm'
import './App.css'

function App() {
  return (
    <>
      <TodoForm />
    </>
  )
}

export default App
