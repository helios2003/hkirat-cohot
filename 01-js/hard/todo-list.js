/*
  Implement a class `Todo` having below methods
    - add(todo): adds todo to list of todos
    - remove(indexOfTodo): remove todo from list of todos
    - update(index, updatedTodo): update todo at given index
    - getAll: returns all todos
    - get(indexOfTodo): returns todo at given index
    - clear: deletes all todos

  Once you've implemented the logic, test your code by running
*/

class Todo {
    constructor() {    // creating and initializing objects in a class
      this.list = []
    }

    // add a todd
    add(id, title, description) {
      const tuple = {id, title, description}
      this.list.push(tuple)
    }

    // remove a todo based on index
    remove(id) {
      this.list = this.list.filter((todo) => {
        return todo.id !== id
      })
    }

    // update a todo
    update(id, title, description) {
      this.list = this.list.map(todo => {
        if (todo.id === id) {
          todo.title = title,
          todo.description = description
        }
        return todo
      })
    }

    // obtain all todos
    getAll() {
      return this.list
    }

    // get todo at a specific index
    get(id) {
      const todo = this.list.filter((todo) => {
        return todo.id === id
      })
      return todo
    }

    // delete all todos
    delete() {
      this.list = []
    }
}

// CHTGPT generated test cases
const todoList = new Todo();

// Add todos
todoList.add(1, "Buy groceries", "Milk, eggs, bread");
todoList.add(2, "Go for a run", "In the park");

// Get all todos and log them
console.log("All Todos:", todoList.getAll());
// Get a specific todo and log it
console.log("Todo at index 1:", todoList.get(1));

// Update a todo
todoList.update(1, "Buy groceries", "Milk, eggs, bread, and cheese");

// Get all todos after updating
console.log("All Todos after update:", todoList.getAll());

// Remove a todo
todoList.remove(2);

// Get all todos after removal
console.log("All Todos after removal:", todoList.getAll());

// Clear all todos
todoList.delete();

// Get all todos after clearing
console.log("All Todos after clearing:", todoList.getAll());

module.exports = Todo;
