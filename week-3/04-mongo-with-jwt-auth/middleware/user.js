const jwt = require('jsonwebtoken');
const env = require('dotenv');

function userMiddleware(req, res, next) {
    // Implement user auth logic
    // You need to check the headers and validate the user from the user DB. Check readme for the exact headers to be expected
    const access = req.headers.authorization;
    access = access.split(' ')[1]; // remove the bearer part
    // saves a db call. Because instead of accessing the db and checking if the user is admin or not, we can just decode 
    // the token and check if the username is admin or not
    try {
        const decoded = jwt.verify(access, env.jwtsecret);
        if (decoded.username && decoded.type === "user") {    // for verifying the admin
            next();
        } else {
            res.status(401).send('stop pretending to be admin!!! Unauthorized access');
        }
    } catch (err) {
        res.status(401).send('Error happened bruhh');
    }
}

module.exports = userMiddleware;