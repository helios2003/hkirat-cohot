const jwt = require('jsonwebtoken');
const env = require('dotenv');
env.config()
// Middleware for handling auth
function adminMiddleware(req, res, next) {
    // Implement admin auth logic
    // You need to check the headers and validate the admin from the admin DB. Check readme for the exact headers to be expected
    // this entrie stuff will bee handled by jwt
    let access = req.headers.authorization;
    access = access.split(' ')[1]; // remove the bearer part
    // saves a db call. Because instead of accessing the db and checking if the user is admin or not, we can just decode 
    // the token and check if the username is admin or not
    try {
        const decoded = jwt.verify(access, process.env.jwt_secret);
        console.log(decoded)
        if (decoded.username && decoded.type === "admin") {    // for authorizing the admin
            next();
        } else {
            res.status(401).send('stop pretending to be admin!!! Unauthorized access');
        }
    } catch (err) {
        console.error(err)
        res.status(401).send('Error happened bruhh');
    }
}

module.exports = adminMiddleware;