const { Router } = require("express");
const admin = require('../db/index')
const jwt = require("jsonwebtoken")
const adminMiddleware = require("../middleware/admin");
const router = Router();
const env = require('dotenv');

env.config();

// Admin Routes
router.post('/signup', async (req, res) => {
    // Implement admin signup logic
    const username = req.body['username']
    const password = req.body['password']
    try {
    const checkAdmin = await admin.Admin.findOne({username: username})
    if (checkAdmin) {
        res.status(405).send("Admin already exists")
    } else {
        const mainguy = new admin.Admin({
            username: username,
            password: password
        })
        mainguy.save()
        res.status(200).send("You are the new admin")
    }
} catch(err) {
        res.status(404).send("Stop pretending to be admin")
    }
});

router.post('/signin', async(req, res) => {
    // Implement admin signin logic -> send the token back to the user
    const username = req.body['username']
    const password = req.body['password']
    const isValid = await admin.Admin.findOne({
        username: username,
        password: password
    })
    if (isValid) {
        const token = jwt.sign({     // thhis is what the browser stoes in the local storage
            username
        }, process.env.jwt_secret)
        res.status(200).json(token);
    } else {
        res.send("bruhhh send correct credentials");
    }
});

router.post('/courses', adminMiddleware, async (req, res) => {
    // Implement course creation logic
    try {
        const existing = await admin.Course.findOne({id: req.body.id})
        if (existing) {
            res.status(400).send("Bruhh same course already exists")
        }
        const post = new admin.Course({
            id: req.body.id,
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            image_url: req.body.image_url,
            published: req.body.published,
        })
        const saveCourse = await post.save()
        res.status(200).json({
            message: "Course created successfully",
            id: saveCourse.id
        })
    } catch(err) {
        res.status(404).send("Error creating course")
    }
});

router.get('/courses', adminMiddleware, async (req, res) => {
    // Implement fetching all courses logic
    try {
        const allCourses = await admin.Course.find()
        res.status(200).json(JSON.stringify(allCourses, undefined, 2))
    } catch(err) {
        res.status(404).send("Error getting course")
    }
});

module.exports = router;