const { Router } = require("express");
const router = Router();
const userMiddleware = require("../middleware/user");
const env = require('dotenv');
const User = require('../db/index')
env.config();

// User Routes
router.post('/signup', (req, res) => {
    // Implement user signup logic
    const username = req.body['username']
    const password = req.body['password']
    try {
        if (username == user.User.findOne({username: username}). // using promises
        then((resolve) => 
        {return resolve})) {
            res.status(409).send("Username already exists")
        } else {
        const new_user = new user.User({
            username: username,
            password: password
        })
        new_user.save()
        res.status(200).send("Yay!! account created")
        }
    } catch(err) {
        res.status(404).send("some error encountered")
    }
});

router.post('/signin', async (req, res) => {
    // Implement user signin logic
    const username = req.body['username']
    const password = req.body['password']
    const isValid = await user.User.findOne({
        username: username,
        password: password
    })
    if (isValid) {
        const token = jwt.sign({     // thhis is what the browser stoes in the local storage
            username
        }, process.env.jwt_secret)
        res.status(200).json(token);
    } else {
        res.send("bruhhh send correct credentials");
    }

});

router.get('/courses', async(req, res) => {
    // Implement listing all courses logic
    try {
        const allCourses = await user.Course.find()
        res.status(200).json(JSON.stringify(allCourses))
    } catch(err) {
        res.status(404).send("Error getting course")
    }
});

router.post('/courses', userMiddleware, async(req, res) => {
    // Implement course purchase logic
    const username = req.body["username"]
    const courseId = req.params.courseId
    try {
        const requiredUser = await user.User.findOne({username: username})
        const array = requiredUser.purchasedCourses
        if (array.includes(courseId)) {
            res.status(400).send("You already purchased this course")
        } else {
            array.push(courseId)
            requiredUser.save()
            res.status(200).send("Course purchased")
        }
    } catch(err) {
        console.error(err)
        res.status(404).send("Error purchasing course")
    }
});

router.get('/purchasedCourses', userMiddleware, async (req, res) => {
    // Implement fetching purchased courses logic
    const username = req.body["username"]
    try {
        const requiredUser = await user.User.findOne({username: username})
        res.status(200).json(requiredUser.purchasedCourses)
    } catch(err) {
        res.status(404).send("Error getting purchased courses")
    }
});

module.exports = router;
