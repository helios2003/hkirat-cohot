const { Router } = require("express");
const admin = require('../db/index')
const adminMiddleware = require("../middleware/admin");
const adminRouter = Router();

// Admin Routes
adminRouter.post('/signup', (req, res) => {
    // Implement admin signup logic
    const username = req.headers['username']
    const password = req.headers['password']
    if (username === 'admin' && password === 'pass') {
        const mainguy = new admin.Admin({
            username: username,
            password: password
        })
        mainguy.save()
        res.status(200).send("You are the new admin")
    } else {
        res.status(404).send("Stop pretending to be admin")
    }
});

adminRouter.post('/courses', adminMiddleware, async (req, res) => {
    // Implement course creation logic
    try {
        const existing = await admin.Course.findOne({id: req.body.id})
        if (existing) {
            res.status(400).send("Bruhh same course already exists")
        }
        const post = new admin.Course({
            id: req.body.id,
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            image_url: req.body.image_url,
            published: req.body.published,
        })
        const saveCourse = await post.save()
        res.status(200).json({
            message: "Course created successfully",
            id: saveCourse.id
        })
    } catch(err) {
        res.status(404).send("Error creating course")
    }
});

adminRouter.get('/courses', adminMiddleware, async (req, res) => {
    // Implement fetching all courses logic
    try {
        const allCourses = await admin.Course.find()
        res.status(200).json(JSON.stringify(allCourses, undefined, 2))
    } catch(err) {
        res.status(404).send("Error getting course")
    }
});

module.exports = adminRouter;