const { Router } = require("express");
const router = Router();
const user = require('../db/index')
const userMiddleware = require("../middleware/user");
const userRouter = Router();

// User Routes
userRouter.post('/signup', async(req, res) => {
    // Implement user signup logic
    const username = req.headers['username']
    const password = req.headers['password']
    try {
    if (username == await user.User.findOne({username: username})) {
        res.status(409).send("Username already exists")
    } else {
    const new_user = new user.User({
        username: username,
        password: password
    })
    new_user.save()
    res.status(200).send("Yay!! account created")
        }
    } catch(err) {
        res.status(404).send("some error encountered")
    }
 }
);

userRouter.get('/courses', userMiddleware, async (req, res) => {
    // Implement listing all courses logic
    try {
        const allCourses = await user.Course.find()
        res.status(200).json(JSON.stringify(allCourses))
    } catch(err) {
        res.status(404).send("Error getting course")
    }
});

userRouter.post('/courses', userMiddleware, async (req, res) => {
    // Implement course purchase logic
    const username = req.headers["username"]
    const courseId = req.params.courseId
    try {
        const requiredUser = await user.User.findOne({username: username})
        const array = requiredUser.purchasedCourses
        if (array.includes(courseId)) {
            res.status(400).send("You already purchased this course")
        } else {
            array.push(courseId)
            requiredUser.save()
            res.status(200).send("Course purchased")
        }
    } catch(err) {
        console.error(err)
        res.status(404).send("Error purchasing course")
    }
});

userRouter.get('/purchasedCourses', userMiddleware, async(req, res) => {
    // Implement fetching purchased courses logic
    const username = req.headers["username"]
    try {
        const requiredUser = await user.User.findOne({username: username})
        res.status(200).json(requiredUser.purchasedCourses)
    } catch(err) {
        res.status(404).send("Error getting purchased courses")
    }
});

module.exports = userRouter;
